<?php

/**
 * @file
 * Display gravatar
 */

class gravatar_views_handler_gravatar extends views_handler_field {

  function construct() {
    parent::construct();
    $this->additional_fields['mail'] = 'mail';
  }
  
  function option_definition() {
    
    $options = parent::option_definition();

    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    
    parent::options_form($form, $form_state);
    
  }

  function render($values) {
    if(isset($values->users_mail)) {
      $path = gravatar_get_gravatar($values->users_mail);
      $output = theme('image', 
        array('path' => $path)
      );

      return $output;
    }
  }
}